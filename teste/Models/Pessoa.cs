using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using System.Globalization;

namespace Teste.Models
{
    
    public class Pessoa
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public DateTime Entrada {get; set;}

        
        

    }
}