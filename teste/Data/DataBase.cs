using System;
using System.Data.SqlClient;

namespace Teste.Data
{
    public abstract class DataBase : IDisposable
    {
        protected SqlConnection connection;
        //private string server ="";
        private string server ="DESKTOP-IS5MGK5";

        private string database = "BDCadastro";

        private bool WindowsAuth = false;

        public DataBase()
        {
            /*string strConn = 
            @"Data Source=172.17.0.2; 
            Initial Catalog=BDCadastro;
            User ID=sa;
            Password=dba";
            */

            
             string strConn = 
            @"Data Source=bdcadastro.cfgzgcqrlplj.sa-east-1.rds.amazonaws.com; 
            Initial Catalog=BDCadastro;
            User ID=admin;
            Password=adminadmin";
            

            connection = new SqlConnection(strConn);
            connection.Open();
        }

        public void Dispose()
        {
            this.connection.Close();
        }

    }
}