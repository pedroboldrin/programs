using System.Collections.Generic;
using System.Data.SqlClient;
using Teste.Models;
using System;

namespace Teste.Data
{
    public class PessoaData : DataBase
    {
        public void Create(Pessoa c)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "exec cadPes @nome, @email,@telefone";

            cmd.Parameters.AddWithValue("@nome", c.Nome);
            cmd.Parameters.AddWithValue("@email", c.Email);
            cmd.Parameters.AddWithValue("@telefone", c.Telefone);

            cmd.ExecuteNonQuery();
        }

        public List<Pessoa> Read()
        {
            List<Pessoa> lista = new List<Pessoa>();
            

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"SELECT * FROM Pessoas";

            SqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                Pessoa pessoa = new Pessoa();
                pessoa.Id = (int)reader["Id"];
                pessoa.Nome = (string)reader["Nome"];
                pessoa.Email = (string)reader["Email"];
                pessoa.Telefone = (string)reader["Telefone"];
                pessoa.Entrada = (DateTime)reader["Entrada"];

                lista.Add(pessoa);
            }
            return lista;
        }
        public Pessoa Read(int id)
        {
            var cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"SELECT * FROM Pessoas WHERE Id = @id";

            cmd.Parameters.AddWithValue("@id", id);

            var reader = cmd.ExecuteReader();
            
            if(reader.Read())
            {
                return new Pessoa
                {
                    Id = (int)reader["Id"],
                    Nome = (string)reader["Nome"],
                    Email = (string)reader["Email"],
                    Telefone = (string)reader["Telefone"],
                    Entrada = (DateTime)reader["Entrada"]
                };
            } 
            return null;
        }

        public void Delete(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "exec delPes @id";

            cmd.Parameters.AddWithValue("@id", id);

            cmd.ExecuteNonQuery(); 
        }

        public void Update(Pessoa p)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"exec upPes @id , @nome, @email, @telefone ";

            
            cmd.Parameters.AddWithValue("@id", p.Id);
            cmd.Parameters.AddWithValue("@nome", p.Nome);
            cmd.Parameters.AddWithValue("@email", p.Email);
            cmd.Parameters.AddWithValue("@telefone", p.Telefone);

            cmd.ExecuteNonQuery();
        }


    }
        

}