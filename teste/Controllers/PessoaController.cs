using Teste.Data;
using Teste.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;



namespace Teste.Controllers
{
    public class PessoaController : Controller
    {
        public ActionResult Index(){
            using(PessoaData data = new PessoaData()){
                return View(data.Read());
            }
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Pessoa p)
        {
            if(!ModelState.IsValid)
                return View(p);
            
            using(PessoaData data = new PessoaData())
                data.Create(p);
                
            return RedirectToAction("Index");
        }

        public ActionResult Update(int id)
        {
            using(var data = new PessoaData()){
                return View(data.Read(id));
            }
        }

        [HttpPost]
        public ActionResult Update(int id, Pessoa p)
        {
            p.Id = id;
            
            if(!ModelState.IsValid)
                return View(p);
            
            using(PessoaData data = new PessoaData())
                data.Update(p);
                
            return RedirectToAction("Index");

        }

        public ActionResult Delete(int id)
        {  
            using(var data = new PessoaData())
                data.Delete(id);
                
        
        return RedirectToAction("Index");
        }
    }
}