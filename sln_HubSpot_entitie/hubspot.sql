Create Table Contact
(
	Id					int				not null		primary key identity,
	ResposavelNegocio	varchar(50)		not null,
	ValorNegocio		decimal(10,2)	not null,
	DataNegocio			varchar(20)		not null,
	VendaNova			varchar(50),
	Recompra			varchar(50)
)
go

insert into Contact values('pedro boldrin', 2500.50, '2020-01-09', 'carro', 'celular')

select * from Contact