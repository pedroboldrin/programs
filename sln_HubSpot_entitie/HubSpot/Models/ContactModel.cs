﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace HubSpot.Models
{
    public class ContactModel
    {

        public int Id { get; set; }
        public string ResponsavelNegocio { get; set; }
        public double ValorNegocio { get; set; }
        public string DataNegocio { get; set; }
        public string VendaNova { get; set; }
        public string Recompra { get; set; }
    }
}
