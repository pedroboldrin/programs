﻿using RestSharp;
using System;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace HubSpot
{
    public class HubspotID
    {
        public void GetOfHubspot(string urlList)
        {
            var client = new RestClient(urlList);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string Json = response.Content;

            JObject principal = JObject.Parse(Json);
            var equipamentos =
                from p in principal["contacts"].Children()
                select p;

            foreach (var equip in equipamentos)
            {
                var comandos = from c in equip["vid"]
                               select c;
                var id = (int)equip["vid"];
                if (id != 1 && id != 51)
                {
                    Console.WriteLine("VIDs: " + id);
                }
            }

        }
    }
}
