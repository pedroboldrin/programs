﻿using HubSpot.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Text;

namespace HubSpot.Data
{

    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {

        public DbSet<ContactModel> contact { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                                    .SetBasePath(Directory.GetCurrentDirectory())
                                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();

            string conectionString = configuration["ConnectionStrings:DefaultConnection"];

            optionsBuilder.UseSqlServer(conectionString);
        }
    }
}
