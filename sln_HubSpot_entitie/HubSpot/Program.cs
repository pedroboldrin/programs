﻿using System;
using HubSpot.Models;
using HubSpot.Data;
using Newtonsoft.Json.Linq;
using RestSharp;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace HubSpot
{
    
    public class Program
    {

        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                                    .SetBasePath(Directory.GetCurrentDirectory())
                                    .AddJsonFile("appsettings.json", optional:true, reloadOnChange:true);
            IConfigurationRoot configuration = builder.Build();

            string urlContact1 = configuration["Variables:ContactById1"];
            string urlContact2 = configuration["Variables:ContactById2"];
            string urlList = configuration["Variables:ContactIdList"];

            HubspotID hub = new HubspotID();
            hub.GetOfHubspot(urlList);

            ContactModel h = new ContactModel();

            Console.WriteLine("Insira o Id do Contato: ");
            string id;
            id = Console.ReadLine();
            string urlContact = $"{urlContact1}{id}{urlContact2}";
            var client = new RestClient(urlContact);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string Json = response.Content;

            JObject jObject = JObject.Parse(Json);
            JToken jUser = jObject["properties"];
            h.ResponsavelNegocio = jUser["responsavel_pelo_negocio"]["value"].ToString();
            h.ValorNegocio = (double)jUser["valor_do_negocio"]["value"];
            h.DataNegocio = jUser["data_do_negocio"]["value"].ToString();
            h.VendaNova = jUser["venda_nova"]["value"].ToString();
            h.Recompra = jUser["recompra"]["value"].ToString();

            using (var context = new DbContext())
            {
                context.contact.Add(h);
                context.SaveChanges();
            }
            Console.WriteLine($"\nResponsavel Pelo Negócio: {h.ResponsavelNegocio}" +
                $"\nValor do Negócio: {h.ValorNegocio}" +
                $"\nData do Negócio: {h.DataNegocio}" +
                $"\nVenda Nova: {h.VendaNova}" +
                $"\nRecompra: {h.Recompra}" +
                $"\n\nFim");
        }
    }
}
