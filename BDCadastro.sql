create database BDCadastro
go



Create Table Pessoas
(
	Id			int				NOT NULL	primary key identity,
	Nome		varchar(60)		NOT NULL,
	Email		varchar(50)		NOT NULL,
	Telefone	varchar(20)		NOT NULL,
	Entrada		datetime
)
go

insert into Pessoas values('Pedro', 'pedro.boldrin@gmail.com', '17991406332', SYSDATETIME())

select * From Pessoas
go

create procedure cadPes
(
     @nome varchar(60),  @email varchar(50), @telefone varchar(20)
)
as
begin
     insert into Pessoas values (@nome, @email, @telefone, SYSDATETIME())
end
go

exec cadPes 'Jonas', 'jonas@gmail.com', '6464646464'
go



create procedure upPes
(
    @id int, @nome varchar(50), @email varchar(50), @telefone varchar(20)
)
as
begin
	 update Pessoas set Nome=@nome, Email=@email, Telefone=@telefone, Entrada=SYSDATETIME() where Id=@id
end
go

exec upPes 1, 'Jo�o Santos', 'Joao.santos@gmail.com', '4545454545'

select * From Pessoas
go


create procedure delPes
(
    @id int
)
as
begin
	 delete from Pessoas where Id=@id
end
go

delete from Pessoas where Id=1



exec delPes 3

select * From Pessoas
go